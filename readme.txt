Zadanie 9
Napisać środowisko dla danego języka programowania implementujące zbiór zmiennych sieciowych typu string.  Środowisko składa się z dwóch rodzajów komponentów:
Komponentu zarządzania zbiorem. Każdy komponent może być podłączony z dowolną liczbą innych komponentów.
Biblioteki programistycznej działającej z danym komponentem

Biblioteka ma udostępniać następujące funkcje.
dhCreate( nazwa ) – alokacja zmiennej
dhFree( nazwa ) – zwolnienie zmiennej. Zmienna jest fizycznie zwalniana, jeśli liczba użytkowników  spadnie do zera.
dhGet( nazwa ) – pobranie wartości zmiennej
dhSet( nazwa, wartosc ) – ustawienie wartości zmiennej
dhSetCallback( nazwa, funkcja ) – ustawienie funkcji callback wywoływanej w momencie zmiany wartości zmiennej sieciowej

Zasady implementacji i oceniania
Projekty przeznaczone są dla zespołów 4-osobowych.  Każdy osoba implementuje zadanie w innym języku programowana/środowisku. Przez różne środowiska rozumiane są: C++ na platformie Windows, Java, C#, C++ na platformie Unix, Python i inne po uzgodnieniu z prowadzącym.

Oceniane są poszczególne osoby, ale bardzo silny wpływ na ocenę ma umiejętność współpracy z pozostałymi osobami. W na ocenę mają wpływ następujące czynniki:
Jakość implementacji
Liczba innych implementacji, z którymi dana implementacja współpracuje.

Zasady są następujące:
Jeżeli dana osoba zrealizuje zadanie w 100%, ale nie uda się go zintegrować z żadną inną implementacją otrzymuje12 p.
Integracja z 1 implementacją dodaje 4 p.
Integracja z 2 implementacjami dodaje 8 p.
Integracja z większą liczbą implementacji niż 2 nie zwiększa dalej punktacji.
Usterki w rozwiązaniu obniżają punktację odpowiednio do ich skali.