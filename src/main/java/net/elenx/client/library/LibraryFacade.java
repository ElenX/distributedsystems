package net.elenx.client.library;

import lombok.AllArgsConstructor;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class LibraryFacade implements LibraryService
{
    @Delegate private final RestManager restManager;
    @Delegate private final CallbackManager callbackManager;
}
