package net.elenx.client.library;

import com.google.api.client.util.Strings;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@CommonsLog
@Component
@AllArgsConstructor
class CallbackManager
{
    private static final String NO_OBJECT = null;
    
    private final Map<String, ValueChangeListener> callbacks = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, String> reminders = Collections.synchronizedMap(new HashMap<>());
    private final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1);
    private final RestManager restManager;
    private final NodesProvider nodesProvider;
    
    @PostConstruct
    private void postConstruct()
    {
        scheduledExecutorService.scheduleAtFixedRate(this::pool, 0, 1, TimeUnit.SECONDS);
    }
    
    @PreDestroy
    private void preDestroy()
    {
        scheduledExecutorService.shutdown();
    }
    
    @Async
    public void setCallback(String id, ValueChangeListener valueChangeListener)
    {
        callbacks.put(id, valueChangeListener);
        reminders.put(id, NO_OBJECT);
    }
    
    @SneakyThrows
    private synchronized void pool()
    {
        reminders
            .keySet()
            .parallelStream()
            .map(this::fetchRemoteEntry)
            .filter(this::changedValue)
            .forEach(this::invokeCallback);
    }
    
    private Map.Entry<String, String> fetchRemoteEntry(String id)
    {
        String value = nodesProvider
            .getCallbackNodes()
            .stream()
            .map(serverAddress -> restManager.get(serverAddress, id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .findAny()
            .orElse(NO_OBJECT);
    
        return new AbstractMap.SimpleEntry<>(id, value);
    }
    
    private boolean changedValue(Map.Entry<String, String> remoteEntry)
    {
        String id = remoteEntry.getKey();
        String localValue = reminders.get(id);
        String remoteValue = remoteEntry.getValue();
    
        if(Strings.isNullOrEmpty(localValue))
        {
            return !Strings.isNullOrEmpty(remoteValue);
        }
        else
        {
            return !localValue.equals(remoteValue);
        }
    }
    
    @SneakyThrows
    private void invokeCallback(Map.Entry<String, String> remoteEntry)
    {
        String id = remoteEntry.getKey();
        String remoteValue = remoteEntry.getValue();
        
        reminders.put(id, remoteValue);
        callbacks.get(id).onChange(remoteValue);
    }
}
