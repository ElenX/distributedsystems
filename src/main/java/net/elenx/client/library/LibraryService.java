package net.elenx.client.library;

import java.util.Optional;

/**
    dhCreate( nazwa ) – alokacja zmiennej
    dhFree( nazwa ) – zwolnienie zmiennej. Zmienna jest fizycznie zwalniana, jeśli liczba użytkowników  spadnie do zera.
    dhGet( nazwa ) – pobranie wartości zmiennej
    dhSet( nazwa, wartosc ) – ustawienie wartości zmiennej
    dhSetCallback( nazwa, funkcja ) – ustawienie funkcji callback wywoływanej w momencie zmiany wartości zmiennej sieciowej
 */
public interface LibraryService
{
    boolean create(String serverAddress, String id);
    
    boolean free(String serverAddress, String id);
    
    Optional<String> get(String serverAddress, String id);
    
    boolean set(String serverAddress, String id, String value);
    
    void setCallback(String id, ValueChangeListener valueChangeListener);
}
