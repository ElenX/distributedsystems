package net.elenx.client.library;

import lombok.AllArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@CommonsLog
@Component
@AllArgsConstructor
class RestManager
{
    private static final String NOT_FOUND = "404 Not Found";
    private static final String FORBIDDEN = "403 Forbidden";
    private static final Object NO_OBJECT = null;
    
    private final RestTemplate restTemplate;
    
    public boolean create(String serverAddress, String id)
    {
        try
        {
            log.info(String.format("Request CREATE [POST] |%s|", id));
            restTemplate.postForObject(serverAddress, NO_OBJECT, Void.class, id);
            log.info(String.format("Response CREATE [POST] |%s| -> OK", id));
            
            return true;
        }
        catch(HttpClientErrorException e)
        {
            if(FORBIDDEN.equals(e.getMessage()))
            {
                log.info(String.format("Response CREATE [POST] |%s| -> Already exists", id));
                
                return false;
            }
            else
            {
                throw e;
            }
        }
    }
    
    public boolean free(String serverAddress, String id)
    {
        try
        {
            log.info(String.format("Request FREE [DELETE] |%s|", id));
            restTemplate.delete(serverAddress, id);
            log.info(String.format("Response FREE [DELETE] |%s| -> OK", id));
        
            return true;
        }
        catch(HttpClientErrorException e)
        {
            if(NOT_FOUND.equals(e.getMessage()))
            {
                log.info(String.format("Response FREE [DELETE] |%s| -> Not found", id));
                
                return false;
            }
            else
            {
                throw e;
            }
        }
    }
    
    public Optional<String> get(String serverAddress, String id)
    {
        try
        {
            log.info(String.format("Request GET [GET] |%s|", id));
            String value = restTemplate.getForObject(serverAddress, String.class, id);
            log.info(String.format("Response GET [GET] |%s| -> |%s|", id, value));
            
            return Optional.ofNullable(value);
        }
        catch(HttpClientErrorException e)
        {
            if(NOT_FOUND.equals(e.getMessage()))
            {
                log.info(String.format("Response GET [GET] |%s| -> Not found", id));
                
                return Optional.empty();
            }
            else
            {
                throw e;
            }
        }
    }
    
    public boolean set(String serverAddress, String id, String value)
    {
        try
        {
            log.info(String.format("Request SET [PUT] id=|%s| value=|%s|", id, value));
            restTemplate.put(serverAddress, value, id);
            log.info(String.format("Response SET [PUT] id=|%s| value=|%s| -> OK", id, value));
            
            return true;
        }
        catch(HttpClientErrorException e)
        {
            if(NOT_FOUND.equals(e.getMessage()))
            {
                log.info(String.format("Response SET [PUT] |%s| -> Not found", id));
                
                return false;
            }
            else
            {
                throw e;
            }
        }
    }
}
