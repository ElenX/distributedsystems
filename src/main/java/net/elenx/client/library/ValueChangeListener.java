package net.elenx.client.library;

public interface ValueChangeListener
{
    void onChange(String value);
}
