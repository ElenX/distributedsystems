package net.elenx.client.library;

import lombok.Data;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Data
@Service
public class NodesProvider
{
    private final Set<String> propagationNodes = new HashSet<>();
    private final Set<String> callbackNodes = new HashSet<>();
    
    /**
     * zbior propagationNodes zawiera adresy serwerow, do ktorych moj serwer rozsyla zmiany
     * zbior callbackNodes zawiera adresy serwerow, z ktorych moj serwer pobiera dane
     *
     * w obu zbiorach mozna, ale nie trzeba podac ten sam adres
     * place holder {id} w adresie zostanie zastapiony id zasobu
     */
    @PostConstruct
    private void postConstruct()
    {
//        propagationNodes.add("http://localhost:8080/{id}");
//        propagationNodes.add("http://www.google.com/whatever/{id}");
//        propagationNodes.add("http://www.facebook.com/SR/{id}");
    
        callbackNodes.add("http://localhost:8080/{id}");
//        callbackNodes.add("http://www.google.com/whatever/{id}");
//        callbackNodes.add("http://www.facebook.com/SR/{id}");
    }
}
