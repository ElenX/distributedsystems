package net.elenx.client.library;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
class LibraryConfiguration
{
    @Bean
    RestTemplate restTemplate()
    {
        return new RestTemplate();
    }
}
