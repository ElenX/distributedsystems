package net.elenx.client;

import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import net.elenx.client.library.LibraryService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@CommonsLog
@ComponentScan
public class Client
{
    @SneakyThrows
    public static void main(String[] args)
    {
        String serverAddress = "http://localhost:8080/{id}";
        
        BeanFactory beanFactory = new AnnotationConfigApplicationContext(Client.class);
        LibraryService libraryService = beanFactory.getBean(LibraryService.class);
    
        log.info("Client started");
    
        libraryService.create(serverAddress, "id");
        libraryService.set(serverAddress, "id", "asd");
        libraryService.setCallback("id", value -> log.info("==================================> " + value));

        for(int i = 0; true; i++)
        {
            Thread.sleep(3000);
            libraryService.set(serverAddress, "id", String.valueOf(i));
        }
    }
}
