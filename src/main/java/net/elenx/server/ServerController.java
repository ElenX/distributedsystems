package net.elenx.server;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Log
@RequiredArgsConstructor
@RestController
class ServerController implements ServerService
{
    private final Map<String, String> data = Collections.synchronizedMap(new HashMap<>());
    private final ServerApi serverApi;
    
    @Override
    @RequestMapping(value="/{id}", method = RequestMethod.POST)
    public void create(@PathVariable("id") String id, HttpServletResponse httpServletResponse)
    {
        log.info("CREATE " + id);
        
        if(data.containsKey(id))
        {
            httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
        else
        {
            data.put(id, null);
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        }
        
        serverApi.create(id);
    }
    
    @Override
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void free(@PathVariable("id") String id, HttpServletResponse httpServletResponse)
    {
        log.info("FREE " + id);
        
        if(data.containsKey(id))
        {
            data.remove(id);
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        }
        else
        {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        
        serverApi.free(id);
    }
    
    @Override
    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public String get(@PathVariable("id") String id, HttpServletResponse httpServletResponse)
    {
        log.info("GET " + id);
        
        if(data.containsKey(id))
        {
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            
            return data.get(id);
        }
        else
        {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
            
            return null;
        }
    }
    
    @Override
    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    public void set(@PathVariable("id") String id, @RequestBody String value, HttpServletResponse httpServletResponse)
    {
        log.info("SET " + id + " " + value);
        
        if(data.containsKey(id))
        {
            data.put(id, value);
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        }
        else
        {
            httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
        
        serverApi.set(id, value);
    }
}
