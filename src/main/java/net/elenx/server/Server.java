package net.elenx.server;

import lombok.extern.java.Log;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@Log
@SpringBootApplication
@ComponentScan({"net.elenx"})
public class Server
{
    public static void main(String[] args)
    {
        SpringApplication springApplication = new SpringApplication(Server.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
        
        log.info("Server accepts requests at: http://localhost:8080/");
    }
}
