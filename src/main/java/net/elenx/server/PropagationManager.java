package net.elenx.server;

import lombok.RequiredArgsConstructor;
import net.elenx.client.library.LibraryService;
import net.elenx.client.library.NodesProvider;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
@RequiredArgsConstructor
class PropagationManager implements ServerApi
{
    private final LibraryService libraryService;
    private final NodesProvider nodesProvider;
    
    @Override
    public void create(String id)
    {
        propagate(node -> libraryService.create(node, id));
    }
    
    @Override
    public void free(String id)
    {
        propagate(node -> libraryService.free(node, id));
    }
    
    @Override
    public String get(String id)
    {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void set(String id, String value)
    {
        propagate(node -> libraryService.set(node, id, value));
    }
    
    private void propagate(Consumer<? super String> consumer)
    {
        nodesProvider.getPropagationNodes().forEach(consumer);
    }
}
