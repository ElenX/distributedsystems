package net.elenx.server;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

public interface ServerService
{
    @RequestMapping(value="/{id}", method = RequestMethod.POST)
    void create(@PathVariable("id") String id, HttpServletResponse httpServletResponse);
    
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    void free(@PathVariable("id") String id, HttpServletResponse httpServletResponse);
    
    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String get(@PathVariable("id") String id, HttpServletResponse httpServletResponse);
    
    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    void set(@PathVariable("id") String id, String value, HttpServletResponse httpServletResponse);
}
