package net.elenx.server;

/**
    dhCreate( nazwa ) – alokacja zmiennej
    dhFree( nazwa ) – zwolnienie zmiennej. Zmienna jest fizycznie zwalniana, jeśli liczba użytkowników  spadnie do zera.
    dhGet( nazwa ) – pobranie wartości zmiennej
    dhSet( nazwa, wartosc ) – ustawienie wartości zmiennej
    dhSetCallback( nazwa, funkcja ) – ustawienie funkcji callback wywoływanej w momencie zmiany wartości zmiennej sieciowej
 */
@SuppressWarnings("unused")
public interface ServerApi
{
    /**
     * POST
     * 200 gdy sie uda
     * 403 gdy juz istnieje
     */
    void create(String id);
    
    /**
     * DELETE
     * 200 gdy sie uda
     * 404 gdy nie istnieje
     */
    void free(String id);
    
    /**
     * GET
     * 200 gdy sie uda i wartosc jako string
     * 404 gdy id nie istnieje
     */
    String get(String id);
    
    /**
     * PUT
     * 200 gdy sie uda
     * 404 gdy nie istnieje (nie zostal stworzony przez create, lub zostal zniszczony przez free)
     */
    void set(String id, String value);
}
