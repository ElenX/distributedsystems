package net.elenx.server

import spock.lang.Specification

import javax.servlet.http.HttpServletResponse

class ServerControllerTest extends Specification
{
    private final HttpServletResponse httpServletResponse = Mock()

    void "second set overrides first one"()
    {
        given:
        ServerService serverService = new ServerController()

        when:
        serverService.create("id", httpServletResponse)
        serverService.set("id", "value1", httpServletResponse)
        serverService.set("id", "value2", httpServletResponse)
        String response = serverService.get("id", httpServletResponse)

        then:
        response == "value2"
    }

    void "freeing variable prevents from accessing it after"()
    {
        given:
        ServerService serverService = new ServerController()

        when:
        serverService.create("id", httpServletResponse)
        serverService.set("id", "value", httpServletResponse)
        serverService.free("id", httpServletResponse)

        String response = serverService.get("id", httpServletResponse)

        then:
        response == null
    }

    void "free id can be claimed again"()
    {
        given:
        ServerService serverService = new ServerController()

        when:
        serverService.create("id", httpServletResponse)
        serverService.set("id", "value", httpServletResponse)
        serverService.free("id", httpServletResponse)

        serverService.create("id", httpServletResponse)
        serverService.set("id", "value2", httpServletResponse)
        String response = serverService.get("id", httpServletResponse)

        then:
        response == "value2"
    }

    void "should return null on get over non existing id"()
    {
        given:
        ServerService serverService = new ServerController()

        when:
        String response = serverService.get("id", httpServletResponse)

        then:
        response == null
    }
}
